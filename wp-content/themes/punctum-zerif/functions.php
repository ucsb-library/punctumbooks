<?php
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles',99);
function child_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	 wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/custom.css', array( $parent_style ));
}
if ( get_stylesheet() !== get_template() ) {
    add_filter( 'pre_update_option_theme_mods_' . get_stylesheet(), function ( $value, $old_value ) {
         update_option( 'theme_mods_' . get_template(), $value );
         return $old_value; // prevent update to child theme mods
    }, 10, 2 );
    add_filter( 'pre_option_theme_mods_' . get_stylesheet(), function ( $default ) {
        return get_option( 'theme_mods_' . get_template(), $default );
    } );
}
function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '...');
        $the_excerpt = implode(' ', $words);
    endif;

    $the_excerpt = '<p>' . $the_excerpt . '</p>';

    return $the_excerpt;
}


//add donate shortcode that links into cart [donate id=""]
function donate($atts, $content = null) {
	$a = shortcode_atts(array (
		'id' => ''
	), $atts );
	
	if (edd_item_in_cart($a['id'])) {
		$s = 'in-cart';
	} 
	
	$link = '<h3>donate to punctum</h3>';
	$link .= '<p>Before you start to download your new book, take this moment to <strong>make a donation to punctum books</strong>, a non-profit independent press. Any amount, no matter the size, is appreciated and will be used to keep our ship of fools afloat. Contributions from dedicated readers will also help us to keep our commons open and to cultivate new work that can\'t find a welcoming port elsewhere. <strong>Vive la open-access.</strong></p>[purchase_link id="'. $a['id'].'" class="donate" text="Donate" price="1" color="blue" style="button"]';
	return '<div class="time donate '.$s.'">'.do_shortcode($link).'</div>';
}

add_shortcode( 'donate', 'donate' );


// [button date="" book="" price="" pay="" free=""]
// In this function we define 3 buttons (Buy, Download, Subscribe) to be displayed on individual publication pages. IL 2019-04-03
function time_func( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'date' => '',
	'book' => '',
	'price' => '',
	'pay-link' => '',
	'free-link' => ''
	), $atts );
    
    $time = strtotime($a['date']);
    $check = strtotime('-6 months');
    $price = types_render_field( "price-for-print", array( ) );
    $book = types_render_field("print-url",array("output"=>"raw"));
    $paidID = types_render_field("download-id",array());
    $freeID = types_render_field("free-download-id",array());
    $binding = types_render_field( "binding-and-size", array( ) );
    $oapen   = types_render_field( "oapen-download-link", array("output"=>"raw"));
    $button =  '<div class="buttons">';

//  Buy Printed Book Button
    $button .= '<div id="print"><h3 class="print">Print</h3>';
    $button .= '$'.$price.'<span class="small">'.$binding.'</span><br/><a  href="'.$book.'" target="_blank"><button class="book">buy book</button></a></div>';
//  ebook button
    $button .= '<div id="ebook"><h3 class="print">E-book</h3>';
    $button .= ' free <span class="small">PDF</span><br/>';
    if ( is_user_logged_in() ){
      $button .= '<a href="'.$oapen.'" download="PunctumBooks.pdf"><button class="book">Download</button></a></div>';
      }else{
      $button .= '<a href="javascript:DownloadAndRedirect(\''.$oapen.'\')"><button class="book">Download</button></a></div>';
    }

//  Subscribe Button
  	$button .= '<div id="subscribe"><h3 class="print">Subscribe</h3>$10/month<br/><span class="small"></span><a href="https://punctumbooks.com/support#punctum-subscribe"><button class="subscribe">subscribe</button></a></div>';
//  end buttons div
    $button .= '</div>';


    if (edd_item_in_cart($paidID)) {
	$s = 'in-cart';
    } else { $s = '';}
    return '<div class="time '.$s.'">'.do_shortcode($button).'</div>';  
}
add_shortcode( 'button', 'time_func' );


//remove decimals to be clean

function punctum_edd_format_amount_decimals( $decimals, $amount ) {
	if( floor( $amount ) == $amount )
		$decimals = 0;
	return $decimals;
}
add_filter( 'edd_format_amount_decimals', 'punctum_edd_format_amount_decimals', 10, 2 );

function pw_edd_hide_from_search( $args ) {
	$args['exclude_from_search'] = true;
	return $args;
}
add_filter( 'edd_download_post_type_args', 'pw_edd_hide_from_search' );


//register connection types
p2p_register_connection_type( array( 
    'name' => 'publication_author',
    'from' => 'people',
    'to' => 'publication',
    'sortable' => 'any',
    'title' => array( 'from' => 'Author of', 'to' => 'Authors' )
) );

p2p_register_connection_type( array( 
    'name' => 'publication_editor',
    'from' => 'people',
    'to' => 'publication',
    'sortable' => 'any',
    'title' => array( 'from' => 'Editor of', 'to' => 'Editors' )
) );

p2p_register_connection_type( array( 
    'name' => 'publication_translator',
    'from' => 'people',
    'to' => 'publication',
    'sortable' => 'any',
    'title' => array( 'from' => 'Translator of', 'to' => 'Translators' )
) );

p2p_register_connection_type( array( 
    'name' => 'publication_contributor',
    'from' => 'people',
    'to' => 'publication',
    'sortable' => 'any',
    'title' => array( 'from' => 'Contributor to', 'to' => 'Contributors' )
) );

p2p_register_connection_type( array( 
    'name' => 'publication_designer',
    'from' => 'people',
    'to' => 'publication',
    'sortable' => 'any',
    'title' => array( 'from' => 'Designer of', 'to' => 'Designers' )
) );

//logged in simple shortcode
function check_user ($params, $content = null){
  //check tha the user is logged in
  if ( is_user_logged_in() ){
 
    //user is logged in so show the content
    return do_shortcode($content);
  }  else {
 
    //user is not logged in so hide the content
    return;
 
  }
 
}
 
//add a shortcode which calls the above function
add_shortcode('loggedin', 'check_user' );


//limitless archive for people
function sort_people_archives($query) {
	if ( $query->is_post_type_archive( 'people' ) && $query->is_archive && $query->is_main_query()  ) {
		/*$query->set('order', 'ASC');
		$query->set('meta_key', 'wpcf-last-name');
		$query->set('orderby', 'meta_value');
		$query->set('posts_per_archive_page', 200);*/
	}
		
}
add_filter('pre_get_posts', 'sort_people_archives');


/**
 * Extend WordPress search to include custom fields
 *
 * http://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    
    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;
   
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

?>