<?php

/**

 * @package zerif

 */

?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php
                $title = types_render_field( "full-title", array( ) );
                if ($title !== '') { 
                    echo $title;
                } else {
                    the_title();
                } //end if ?></h1>


	</header><!-- .entry-header -->
	<div class="entry-content">
	<?php
	if (the_content()) {
		echo '<p class="bio">';
		the_content();
		echo '</p>';
	} else {
		
	}
	
	//get author
			$author = new WP_Query( array(
				'connected_type' => 'publication_author',
				'connected_items' => get_queried_object(),
				'nopaging' => true,
			      ) );
		      
		      // Display connected pages
		      if ( $author->have_posts() ) { ?>
			<h3>Authored:</h3>
			<ul>
			<?php while ( $author->have_posts() ) : $author->the_post(); ?>
			    <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			<?php
		      // Prevent weirdness
		      wp_reset_postdata();
		      } 
		      
		      $editor = new WP_Query( array(
			'connected_type' => 'publication_editor',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $editor->have_posts() ) { ?>
			<h3>Edited:</h3>
			<ul>
			<?php while ( $editor->have_posts() ) : $editor->the_post(); ?>
			    <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			<?php
		      // Prevent weirdness
		      wp_reset_postdata();
		      }
		      
		      $cont = new WP_Query( array(
			'connected_type' => 'publication_contributor',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $cont->have_posts() ) : ?>
			<h3>Contributor:</h3>
			<ul>
			<?php while ( $cont->have_posts() ) : $cont->the_post(); ?>
			    <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			<?php
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
		      
		      //get Translators and Designers
		      
		      $trans = new WP_Query( array(
			'connected_type' => 'publication_translator',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $trans->have_posts() ) : ?>
			<h3>Translated:</h3>
			<ul>
			<?php while ( $trans->have_posts() ) : $trans->the_post(); ?>
			    <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			<?php
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
		      
		      $design = new WP_Query( array(
			'connected_type' => 'publication_designer',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $design->have_posts() ) : ?>
			<h3>Designed:</h3>
			<ul>
			<?php while ( $design->have_posts() ) : $design->the_post(); ?>
			    <li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); the_title(); ?></a></li>
			<?php endwhile; ?>
			</ul>
			<?php
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
	?>
		      

	
		<?php

			wp_link_pages( array(

				'before' => '<div class="page-links">' . __( 'Pages:', 'zerif-lite' ),

				'after'  => '</div>',

			) );

		?>

	</div><!-- .entry-content -->



	<footer class="entry-footer">

			
		<div class="related">
			<?php if ($set !== false) { if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } }?>
		</div>	
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

