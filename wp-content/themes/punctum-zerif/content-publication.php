<?php

/**

 * @package zerif

 */

?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="cover">
		<?php the_post_thumbnail( 'large' );
		 $thumbnail_id    = get_post_thumbnail_id($post->ID);
		$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

		if (!empty($thumbnail_image[0]->post_excerpt)) {
			echo '<div class="front-caption">'.$thumbnail_image[0]->post_excerpt.'</div>';
		} else {
		} //end if ?>
		<?php
		$citation = types_render_field("citation",array());
		echo '<div class="citation">'.$citation.'</div>';
		
		$endorse = types_render_field( "endorsement", array( ) );
		if ($endorse !== '') { ?>
		<div class="entry-feature">
			<?php echo $endorse; ?>
		</div>
		<?php } else if(has_term('','imprint')) { ?>
		<div class="related">
			<ul class="imprint">
				<?php if(has_category('journal')){ 
					echo '<h3>other issues</h3>';
				} else { ?>
				<h3>also in this series</h3>
				<?php } //endif ?>
			<?php $term = get_the_terms($post->ID, 'imprint');
			$t = $term[0]->slug;
			$args = array(
				      'posts_per_page' => 3,
				      'post_type'=>'publication',
				      'imprint'=>$t,
				      'exclude'=>$post->ID
				      );
			$imprintlist = get_posts($args);
			foreach ($imprintlist as $i) {
				setup_postdata($i); ?>
				<li><a href="<?php echo get_post_permalink($i->ID); ?>"><div class="thumb"><?php echo get_the_post_thumbnail( $i->ID, 'thumbnail' );  ?></div><?php echo get_the_title($i->ID); ?></a></li>
			<?php
			}//end foreach
			wp_reset_postdata();
			?>
			</ul>
		</div>
		<?php } else { ?>
		<div class="related">
          <?php //related_entries(); ?>
          <?php echo do_shortcode('[yarpp]'); ?>
		</div>
		<?php
		$set = false; } ?>
	</div>
	<header class="entry-header">
		<h1 class="entry-title"><?php
                $title = types_render_field( "full-title", array( ) );
                if ($title !== '') { 
                    echo $title;
                } else {
                    the_title();
                } //end if ?></h1>


	</header><!-- .entry-header -->

	<div class="entry-meta">
                    <?php
		    //Imprint
		    $imprint = get_the_term_list($post->id,'imprint',' ',', ','');
			if (strlen($imprint) > 1) {
				$imprint = str_replace('/imprint/', '/imprints/', $imprint);
				echo '<p>Imprint: '.$imprint.'</p>';	
			} 
		    
		    //Authors and Editors
		    
		    //Backup strings
		    $editors = get_the_term_list( $post->id, 'editor', ' ', ', ', '' );
		    $authors = get_the_term_list( $post->id, 'contributor', ' ', ', ', '' );
		    $about_string = '';
		    $a_test;
		    $e_test;
		    
			//get author
			$author = new WP_Query( array(
				'connected_type' => 'publication_author',
				'connected_items' => get_queried_object(),
				'nopaging' => true,
			      ) );
		      $bio = array();
		      // Display connected pages
		      if ( $author->have_posts() ) {
			if  ($author->post_count > 1) {
				$a_test=1;
			      } else {
				$a_test=2;
			      }
			while ( $author->have_posts() ) : $author->the_post();
				$c = get_the_content();
				array_push($bio,$c);
			endwhile;
			p2p_list_posts( $author, array(
			    'before_list' => '<p>by ',
			    'after_list' => '</p>',
			    'separator'   => ', '
			) );
		      // Prevent weirdness
		      wp_reset_postdata();
		      } else {
			
			/*if (strlen($authors) > 1) {
				echo '<p>by '.$authors.'</p>';
			    }*/
		      }
		      
		      $editor = new WP_Query( array(
			'connected_type' => 'publication_editor',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $editor->have_posts() ) {
			if  ($editor->post_count > 1) {
				$e_test=1;
			      } else {
				$e_test=2;
			      }
			while ( $editor->have_posts() ) : $editor->the_post();
				$c = get_the_content();
				array_push($bio,$c);
			endwhile;
			p2p_list_posts( $editor, array(
			    'before_list' => '<p>Edited by ',
			    'after_list' => '</p>',
			    'separator'   => ', '
			) );
		      // Prevent weirdness
		      wp_reset_postdata();
		      } else {
			/*if (strlen($editors) > 1) {
			    echo '<p>Edited by '.$editors.'</p>';
			}*/
		      }
		      
		      //get Contributors
		      $contributors = new WP_Query( array(
			'connected_type' => 'publication_contributor',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $contributors->have_posts() ) :
			p2p_list_posts( $contributors, array(
			    'before_list' => '<p class="contributors">Contributors ',
			    'after_list' => '</p>',
			    'separator'   => ', '
			) );
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
		      
		      //get Translators and Designers
		      
		      $trans = new WP_Query( array(
			'connected_type' => 'publication_translator',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $trans->have_posts() ) :
			p2p_list_posts( $trans, array(
			    'before_list' => '<p>Translation by ',
			    'after_list' => '</p>',
			    'separator'   => ', '
			) );
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
		      
		      $design = new WP_Query( array(
			'connected_type' => 'publication_designer',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
		      ) );
		      
		      // Display connected pages
		      if ( $design->have_posts() ) :
			p2p_list_posts( $design, array(
			    'before_list' => '<p>Designed by ',
			    'after_list' => '</p>',
			    'separator'   => ', '
			) );
		      // Prevent weirdness
		      wp_reset_postdata();
		      endif;
		      
		      //get about string
			  $b_test = sizeof($bio);
		      if ($a_test > 0 && $e_test > 0) {
				$about_string = 'Contributors';
		      } else if ($a_test > 0) {
			if ($b_test === 1) {
				$about_string = 'Author';
			} else {
				$about_string = 'Authors';
			}
		      } else {
			if ($b_test === 1) {
				$about_string ='Editor';
			} else {
				$about_string = 'Editors';
			}
		      }
		    
		    /*$override = types_render_field("other-byline", array());
			if ($override !=='') {
			echo '<p class="other">'.$override.'</p>';
			}*/
		    
	
		    //Publish Date
                    if (has_category('forthcoming')) {
                        $date = 'FORTHCOMING '.types_render_field( "forthcoming-date", array( ) );
                    } else {
                        $date = 'Published: '.types_render_field( "publish-date", array( "format"=>"m/d/Y" ) );
                    }
		    
		    echo '<p>'.$date.'</p>';
		    
		    
                    ?>
		
		
			
                
		<?php if (!has_category('forthcoming') ) { ?>
                <div class="entry-buy">
			<h2>Buy this title</h2>
			<?php
			$button = '[button date="10/31/2015" book="https://www.createspace.com/5759478" pay-link="4714" free-link="120"]';
			
			//echo $button;
			echo do_shortcode($button); ?>
                </div>
		<?php } //endif forthcoming ?>

	</div>


	<div class="entry-content">
		<div class="entry-post">
			<?php the_content(); ?>
		</div>
		<?php $author = types_render_field( "about-the-author", array( ) );
		
		if (strlen($author) > 1) { //if has author ?>
		<div class="entry-author">
			<h2>About the <?php echo $about_string; ?></h2>
				<?php echo $author; ?>
				
		</div>
		<?php } else { ?>
		<div class="entry-author">
			<?php if ($bio[0] !== '') { ?>
			<h2>About the <?php echo $about_string; ?></h2>
			<?php foreach($bio as $b) {
				echo '<p>'.$b.'</p>';
			}
				} //end of bio?>
			</div>
		<?php }
		//endif ?>
		<?php

			wp_link_pages( array(

				'before' => '<div class="page-links">' . __( 'Pages:', 'zerif-lite' ),

				'after'  => '</div>',

			) );

		?>

	</div><!-- .entry-content -->



	<footer class="entry-footer">

		<?php

			/* translators: used between list items, there is a space after the comma */

			$category_list = get_the_term_list( $post->ID, 'genre', '', ', ', ''  );



			/* translators: used between list items, there is a space after the comma */

			$tag_list = get_the_tag_list( '', __( ', ', 'zerif-lite' ) );



			if ( ! zerif_categorized_blog() ) {

				// This blog only has 1 category so we just need to worry about tags in the meta text

				if ( '' != $tag_list ) {

					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'zerif-lite' );

				} else {

					$meta_text = __( 'Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'zerif-lite' );

				}



			} else {

				// But this blog has loads of categories so we should probably display them here

				if ( '' != $tag_list ) {

					$meta_text = __( 'Genres: %1$s<br/> Tags: %2$s.<br/> Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'zerif-lite' );

				} else {

					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'zerif-lite' );

				}



			} // end check for categories on this blog

			echo '<div class="meta">';

			printf(

				$meta_text,

				$category_list,

				$tag_list,

				get_permalink()

			);
			
			echo '</div>';
		?>



		<?php edit_post_link( __( 'Edit', 'zerif-lite' ), '<span class="edit-link">', '</span>' ); ?>
	
		<div class="related">
			<?php if ($set !== false) { if ( function_exists( "get_yuzo_related_posts" ) ) { get_yuzo_related_posts(); } }?>
		</div>	
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

