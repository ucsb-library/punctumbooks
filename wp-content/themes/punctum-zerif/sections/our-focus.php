<section class="focus" id="focus">



<div class="container">





	<!-- SECTION HEADER -->



	<div class="section-header">






		<!-- SECTION TITLE -->


		<?php
		$zerif_ourfocus_title = get_theme_mod('zerif_ourfocus_title',__('FEATURES','zerif-lite'));
		
		if( !empty($zerif_ourfocus_title) ):
			echo '<h2 class="dark-text">'.__($zerif_ourfocus_title,'zerif-lite').'</h2>';
		endif;	
		?>




		<?php

		$zerif_ourfocus_subtitle = get_theme_mod('zerif_ourfocus_subtitle',__('What makes this single-page WordPress theme unique.','zerif-lite'));



		if( !empty($zerif_ourfocus_subtitle) ):



			echo '<h6>'.__($zerif_ourfocus_subtitle,'zerif-lite').'</h6>';



		endif;

		?>



	</div>





	<div class="row">



			<?php
                        
                        $args = array( 'numberposts' => '4', 'category' => 'featured', 'post-type'=>'titles' );
                        $recent_posts = wp_get_recent_posts( $args ); 
                        foreach( $recent_posts as $recent ){
            
                            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                            $url = $thumb['0'];
                            ?>

			 <div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

                            <div class="service-icon">
				
					<a href="<?php echo get_permalink($recent['ID']); ?>"><i class="pixeden" style="background:url(<?php echo $url; ?>) no-repeat center;width:100%; height:100%;"></i> <!-- FOCUS ICON--></a>
                            </div>
                
                        </div>
                         <?php } //end foreach ?>



</div> <!-- / END CONTAINER -->



</section>  <!-- / END FOCUS SECTION -->