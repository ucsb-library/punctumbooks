<?php
/**
 * The Template for displaying all single titles.
 *
 * @package zerif
 */
get_header(); ?>
<div class="clear"></div>
<script type="text/javascript">
// DEV-1349 clicking download button results in download and redirect to the donate page
function DownloadAndRedirect(DownloadURL)
{
   var RedirectURL = "/support";
   var RedirectPauseSeconds = 2;
   location.href = DownloadURL;
   setTimeout("DoTheRedirect('"+RedirectURL+"')",parseInt(RedirectPauseSeconds*1000));
}
function DoTheRedirect(url) { window.location=url; }
</script>
</header> <!-- / END HOME SECTION  -->
<div id="content" class="site-content">
<div class="container">
<div class="content-left-wrap col-md-9">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php while ( have_posts() ) : the_post(); 
				
				 get_template_part( 'content', 'publication' );
				 
				 //zerif_post_nav(); 
			 
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template('');
				endif;
			endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<div class="sidebar-wrap col-md-3 content-left-wrap">
	<?php get_sidebar(); ?>
</div><!-- .sidebar-wrap -->
</div>
<?php get_footer(); ?>